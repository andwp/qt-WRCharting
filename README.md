# qt-WRCharting

#### 项目介绍
使用QT开发的跨平台无线电相关基本图形，主要包括频谱图，瀑布图，波形图。  
`如果喜欢，请点个start吧！`

##### 频谱图
![avatar](doc/频谱图.gif)
##### 纵向瀑布
![avatar](doc/纵向瀑布图.gif)
##### 横向瀑布
![avatar](doc/横向瀑布图.gif)
##### 波形图
![avatar](doc/波形图.gif)
##### 宽带频谱图 
![avatar](doc/宽带频谱.gif)
#### 软件架构
软件架构说明


#### 安装教程
1. 打开src\WRCharting\WRCharting.pro项目编译  
2. 打开src\Testing\Testing.pro 项目编译后直接运行

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
 